//
//  TableViewController.swift
//  Kadonashi
//
//  Created by SHOKI TAKEDA on 12/5/15.
//  Copyright © 2015 handsomeslot.com. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController, UISearchResultsUpdating, NADViewDelegate {
    
    var selectedText:String = ""
    
    let tableData = [
        "元カレがしつこくて断りたいけど、学校で悪い噂ばらまくぞって脅されて断り方がわからない時",
        "付き合っている人がいるから連絡してこないで",
        "学校に行きたくない",
        "「絵、下手だね」",
        "飲み会でその人の隣は避けたい",
        "話し声が小さくて聞き取れない",
        "ヘアサロンでドライヤーとかシャンプーしてる時にしゃべり出す美容師",
        "寂しい",
        "Hしたくない",
        "タバコを吸わないでほしい",
        "二人で遊ぶことを誘われたけど、みんな含めてなら遊んでも良い",
        "あなたと居ても楽しくない",
        "いつも自分のことしか考えてないよね",
        "最近奥さんが亡くなった方がいて、娘のように可愛がってくれるのはうれしいけど毎日電話がかかってくるのでやめてほしい",
        "打ち上げなんかいってられるか",
        "そういう言い方されたら傷つくんですよ",
        "なんかいつもプラン立てるの私だけなんだけど少し協力してくれない？",
        "会いたくない",
        "あなたと一緒にいたら精神的に疲れる",
        "どこに住んでるとかプライベートなこと教えたくない",
        "辛い・帰りたい・辞めたい・面倒臭い",
        "早く帰りたい",
        "なんか重くてしんどくなってきた",
        "後輩からいきなりタメ語で話された",
        "好きな人が自分の嫌いな人とばっかりしゃべって自分にかまってくれない",
        "あんまり関わりたくない友達に毎月「シフト出た？シフト表送って！遊ぼう!」って催促される",
        "担任が体を触ってくる",
        "気が使えないね",
        "食べ方汚い！",
        "もう帰りたい",
        "うるさい",
        "「普通」◯◯でしょ？",
        "生理的に無理",
        "顔、怖い！",
        "チームワークがなってなくて足手まとい",
        "マナーがなってない",
        "全体的にキツイ",
        "ほんとに無理",
        "なんか臭いんだけど",
        "今日はめんどくさい",
        "上司のいいなりすぎて犬みたい",
        "おまえ、風呂入れよ！",
        "飲み物、食べ物をあげたくない(関節キスが嫌)とき",
        "めっちゃLINEが長く続いてて、もうやめたい",
        "すんげえきらい",
        "食後相手の歯にのりとかがついてる時は？",
        "おもしろくない！！さむい！！",
        "しったかぶってくるのうざいよ",
        "好きです",
        "人に色々言う割には自分もあまり(仕事が)できてないよね！！",
        "うざい",
        "人としてダメ",
        "性欲高すぎ",
        "汚されそうだしパクられそうだから、漫画貸したくない",
        "話がやたら長い",
        "マザコンきめえ",
        "鼻毛でてるよ！",
        "自慢話ばっかりでうんざり", 
        "口、くさい",
        "調子乗るな",
        "ただの遊びなんだけど",
        "お金の使い方が下手すぎ",
        "気づいてないと思うけど、あなた結構嫌われてるよ",
        "早漏すぎだろお前",
        "家に入れたくない",
        "死んだほうがマシだろ",
        "誘いに対して、正直乗り気じゃない",
        "恋愛対象では無くなった",
        "友達の鼻くそが、誰が見ても見えるくらいでてる", 
        "あの子とじゃなくて私とデートして！", 
        "異性に連絡先を聞かれた時の断り方"]
    var filteredTableData = [String]()
    var resultSearchController = UISearchController()
    
    private var nadView: NADView!
    var AdView:UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AdView = UIView()
        
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundView = nil
        tableView.rowHeight = 80
        tableView.backgroundColor = UIColor.clearColor()
        tableView.separatorColor = UIColor.clearColor()
        let imageView = UIImageView(frame: CGRectMake(0, 0, myBoundSize.width, myBoundSize.height))
        let image = UIImage(named: "backView")
        imageView.image = image
        imageView.alpha = 1.0
        tableView.backgroundView = imageView
        
        self.resultSearchController = ({
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.dimsBackgroundDuringPresentation = false
            controller.searchBar.sizeToFit()
            
            self.tableView.tableHeaderView = controller.searchBar
            
            return controller
        })()
        
        self.tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (self.resultSearchController.active) {
            return self.filteredTableData.count
        }
        else {
            return self.tableData.count
        }
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "Cell")
//        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        
        cell.backgroundColor = UIColor.clearColor()
        let cellSelectedBgView = UIView()
        cellSelectedBgView.backgroundColor = UIColor.clearColor()
        cell.selectedBackgroundView = cellSelectedBgView
        cell.contentView.backgroundColor = UIColor.clearColor()
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.font = UIFont(name: "07LogoTypeGothic7", size: 15)
        cell.textLabel?.textColor = UIColor.whiteColor()
        
        if (self.resultSearchController.active) {
            cell.textLabel?.text = filteredTableData[indexPath.row]
            
            return cell
        }
        else {
            cell.textLabel?.text = tableData[indexPath.row]
            
            return cell
        }
    }
    
    override func tableView(table: UITableView, didSelectRowAtIndexPath indexPath:NSIndexPath) {
        let cell:UITableViewCell = self.tableView.cellForRowAtIndexPath(indexPath)!
        selectedText = (cell.textLabel?.text)!
        performSegueWithIdentifier("shift",sender: nil)
    }
    
    override func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if (self.AdView == nil) {
            self.AdView = UIView(frame:CGRectMake(0, 0, self.view.frame.size.width, 50))
            let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
            nadView = NADView(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
            nadView.setNendID("6011089db7691d02c3c9c4e6092c7be61b8714a2",
                spotID: "475395")
            nadView.isOutputLog = false
            nadView.delegate = self
            nadView.load()
            self.AdView!.addSubview(nadView)
            self.AdView!.bringSubviewToFront(nadView)
        }
        return self.AdView
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if segue.identifier == "shift" {
            let resultVC:ResultViewController = segue.destinationViewController as! ResultViewController
            resultVC.topicLabel = selectedText
        }
    }
    
    func updateSearchResultsForSearchController(searchController: UISearchController)
    {
        filteredTableData.removeAll(keepCapacity: false)
        
        let searchPredicate = NSPredicate(format: "SELF CONTAINS[c] %@", searchController.searchBar.text!)
        let array = (tableData as NSArray).filteredArrayUsingPredicate(searchPredicate)
        filteredTableData = array as! [String]
        
        self.tableView.reloadData()
    }
    
}