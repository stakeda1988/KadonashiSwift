//
//  ResultViewController.swift
//  Kadonashi
//
//  Created by SHOKI TAKEDA on 12/5/15.
//  Copyright © 2015 handsomeslot.com. All rights reserved.
//

import UIKit
import MessageUI

class ResultViewController: UIViewController, NADViewDelegate, MFMailComposeViewControllerDelegate {
    var adTimer:NSTimer!
    @IBOutlet weak var mainLabelWidth: NSLayoutConstraint!
    
    @IBOutlet weak var mailBtn: UIButton!
    @IBAction func mailUp(sender: UIButton) {
        let mailViewController = MFMailComposeViewController()
        let toRecipients = ["testflights2015@gmail.com"]
        
        mailViewController.mailComposeDelegate = self
        mailViewController.setSubject("新規角ナシリクエスト")
        mailViewController.setToRecipients(toRecipients) //宛先メールアドレスの表示
        mailViewController.setMessageBody("", isHTML: false)
        
        self.presentViewController(mailViewController, animated: true, completion: nil)
        
    }
    @IBOutlet weak var mainLabel: UILabel!
    private var nadView: NADView!
    var topicLabel:String = ""
    let resultText:[[String]] = [
        ["元カレがしつこくて断りたいけど、学校で悪い噂ばらまくぞって脅されて断り方がわからない時", "(^-^)←無言の圧力\r\r(・ω・)←軽蔑の眼差し"],
    ["付き合っている人がいるから連絡してこないで","ごめん今携帯水没してて確認できないんだよね！"],
    ["学校に行きたくない", "五教科とは別の、どうしてもやりたい勉強があって、学校で過ごす時間がもったいない。家で勉強しててもいい？"],
    ["「絵、下手だね」", "「左手で描いたの？(^-^)」"],
    ["飲み会でその人の隣は避けたい", "「え、その話なに？」って少し離れた所にいる人たちの話題に参加し「へー！それでそれで？」と言いながら席を移動する"],
    ["話し声が小さくて聞き取れない", "「え、なんて？わたし、耳めっちゃ悪いんだよね！なになに？」"],
    ["ヘアサロンでドライヤーとかシャンプーしてる時にしゃべり出す美容師", "ドライヤー⇒雑誌を読む\r\rシャンプー⇒全ての質問に「いや」「まあ」「はい」の３つで返す"],
    ["寂しい", "「あなた不足」"],
    ["Hしたくない", "膀胱炎になった"],
    ["タバコを吸わないでほしい", "めっちゃ咳き込む"],
    ["二人で遊ぶことを誘われたけど、みんな含めてなら遊んでも良い", "タコパしたい！タコパにしようよー！\r\r※人数が要る遊びを提案"],
    ["あなたと居ても楽しくない", "なんか、斬新な遊びがしたい！\r良いプラン思いついたら教えて！"],
    ["いつも自分のことしか考えてないよね", "ねー、それ一緒に考えてあげるよ！\rだから私のも一緒に考えてー"],
    ["最近奥さんが亡くなった方がいて、娘のように可愛がってくれるのはうれしいけど毎日電話がかかってくるのでやめてほしい", "LINEしよ！LINE好きなんだよね、私"],
    ["打ち上げなんかいってられるか", "さっき昂りすぎて実はちょっとチビッた！\r早くパンツ履き替えたい！今日は帰るね！"],
    ["そういう言い方されたら傷つくんですよ", "オブラートお願いします"],
    ["なんかいつもプラン立てるの私だけなんだけど少し協力してくれない？", "「ねー、Aちゃん、これ調べてー\rあ、Bくん、ここ予約してー！」\rって独断で係りを振り分ける。分担。"],
    ["会いたくない", "ニンニク食べちゃった"],
    ["あなたと一緒にいたら精神的に疲れる", "あなた元気すぎるから、\r私がもっと元気な時に改めて会おう\r\ror\r\rまじ妖怪メンタルクラッシャーだな"],
    ["どこに住んでるとかプライベートなこと教えたくない", "それ、デリケートゾーン(　^ω^)"],
    ["辛い・帰りたい・辞めたい・面倒臭い", "メンタルブレイクしました"],
    ["早く帰りたい", "今日人と待ち合わせしてるから、\rまた明日ねー！"],
    ["なんか重くてしんどくなってきた", "一緒に居ても楽しいより辛いの方が増えてきた\rなんか最近、嬉しいより悲しいの方が多い"],
    ["後輩からいきなりタメ語で話された", "ということは、しっかりする必要も無いから\rとことん甘える"],
    ["好きな人が自分の嫌いな人とばっかりしゃべって自分にかまってくれない", "構ってくれないというか、\r話に入れてないだけだから、\r自分から話に割って入る「え、何が？」「それでそれで！」「ねー、なにそれ！！」って、\rどんな話題でも参加しまくる"],
    ["あんまり関わりたくない友達に毎月「シフト出た？シフト表送って！遊ぼう!」って催促される", "「シフト出たんだけど、今月無理そうだ！\rまた来月あたりで調整しよう！」を繰り返す\r三ヶ月程度で、疎遠になり始めると思うよ！"],
    ["担任が体を触ってくる", "もう二歩下がって\r手が届かない距離から話をしよう"],
    ["気が使えないね", "自分のことで、\rいっぱいいっぱいに生きてるもんね"],
    ["食べ方汚い！", "インドと何かあった？元彼インド人？\rあ、なんか食べ方がそんな感じしたから！\r手づかみも慣れ親しんでそうだなって"],
    ["もう帰りたい", "明日、早いんだよねー"],
    ["うるさい", "マスク、使う？"],
    ["「普通」◯◯でしょ？", "異例だね"],
    ["生理的に無理", "大好きなんだけど、\r家族みたいな感じなんだよね"],
    ["顔、怖い！", "肝試しやるとき、呼びたい！"],
    ["チームワークがなってなくて足手まとい", "存在感あり過ぎるから、\rもうちょい馴染む方向で！"],
    ["マナーがなってない", "もしかして帰国子女だったり？\rなんか感情の起伏が、\r文化レベルで独特だなぁって"],
    ["全体的にキツイ", "とりあえず、総入れ替えしてみるのが良さそう"],
    ["ほんとに無理", "今は厳しい"],
    ["なんか臭いんだけど", "体調崩してるっぽいよ！"],
    ["今日はめんどくさい", "別日でもいい？"],
    ["上司のいいなりすぎて犬みたい", "NOと言えない日本人選手権があったら、\rうちの会社の代表で出場してほしい\r絶対勝てる"],
    ["おまえ、風呂入れよ！", "最後いつお風呂入った？\r\ror\r\r一番風呂、譲るよー！"],
    ["飲み物、食べ物をあげたくない(関節キスが嫌)とき", "あ、私今風邪ひいてるから、ダメだ！"],
    ["めっちゃLINEが長く続いてて、もうやめたい", "キラキラした表情のスタンプ"],
    ["すんげえきらい", "アレルギー出ちゃうんだよね"],
    ["食後相手の歯にのりとかがついてる時は？", "なんかもっと食べさせる\r沢山食べれば、そのうち取れる"],
    ["おもしろくない！！さむい！！", "え？ちょっと見逃したかも、もっかいやって？ "],
    ["しったかぶってくるのうざいよ", "ね、ほんとに知ってる？\rその情報、信頼できる？\rそれでいく？\rファイナルアンサー？ "],
    ["好きです", "グッと来てます "],
    ["人に色々言う割には自分もあまり(仕事が)できてないよね！！", "同志ですね！"],
    ["うざい", "まじここウーザス学園だわー"],
    ["人としてダメ", "猫だったら、良かったのにね"],
    ["性欲高すぎ", "いつも大体理性が失われてる状態だからあれだけど、\r通常の性格って、\rどんなんなの？\r会ってみたいんだけど"],
    ["汚されそうだしパクられそうだから、漫画貸したくない", "これ保管用なんだよね\r読む用は今人に貸してる"],
    ["話がやたら長い", "要点教えて"],
    ["マザコンきめえ", "お母さんに勝てる気がしないから、\r私は棄権するね"],
    ["鼻毛でてるよ！", "「激辛ラーメン食べよー」\r\r↓\r\r「あ、鼻かむ？はい、ティッシュ」"],
    ["自慢話ばっかりでうんざり", "「武勇伝武勇伝、\r武勇伝デンデデンデン、\rレッツゴー！」\rと煽る"],
    ["口、くさい", "ちょっと、距離近い。"],
    ["調子乗るな", "いったん落ち着こう。\rまだリスク対策が不完全だし、ね。"],
    ["ただの遊びなんだけど", "やー、楽しかったねー、\rまた機会があったら遊ぼうね！"],
    ["お金の使い方が下手すぎ", "お金、預かろうか？\r買い物、付き合おうか？"],
    ["気づいてないと思うけど、あなた結構嫌われてるよ", "今のメンバーには\r適合してない感じがあるから、\rコミュニティを変えたほうがいいかも。"],
    ["早漏すぎだろお前", "もっかい。"],
    ["家に入れたくない", "犬のしつけがまだ終わってなくて、\r噛むんだよね"],
    ["死んだほうがマシだろ", "打たれ強いよね"],
    ["誘いに対して、正直乗り気じゃない", "締め切り仕事があって、\rそれが順調に終われば行けるんだけど、\r難航した場合、無理なんだよね"],
    ["恋愛対象では無くなった", "今は、もっと、夢のために頑張りたいの"], 
    ["友達の鼻くそが、誰が見ても見えるくらいでてる", "こより使って、\r誰が一番くしゃみを我慢できるか競争しよー！"], 
    ["あの子とじゃなくて私とデートして！", "私も会いたい"], 
    ["異性に連絡先を聞かれた時の断り方", "何用？"]
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        mainLabelWidth.constant = myBoundSize.width - 20
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-50, width: 320, height: 50))
        nadView.setNendID("6011089db7691d02c3c9c4e6092c7be61b8714a2",
            spotID: "475395")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
        
        mainLabel?.font = UIFont(name: "07LogoTypeGothic7", size: 15)
        mailBtn.titleLabel?.font = UIFont(name: "07LogoTypeGothic7", size: 15)
        mainLabel.baselineAdjustment = UIBaselineAdjustment.AlignCenters
        
        adTimer = NSTimer.scheduledTimerWithTimeInterval(4.0, target: self, selector: Selector("onAdStart"), userInfo: nil, repeats: true)
        for i in 0...resultText.count {
            if topicLabel == resultText[i][0] {
                mainLabel.text = resultText[i][1]
                break
            } else {
            }
        }
    }
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func onAdStart() {
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView.removeFromSuperview()
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-50, width: 320, height: 50))
        nadView.setNendID("6011089db7691d02c3c9c4e6092c7be61b8714a2",
            spotID: "475395")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
    }
    
    override func viewDidAppear(animated: Bool) {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "onOrientationChange:", name: UIDeviceOrientationDidChangeNotification, object: nil)
    }
    
    // 端末の向きがかわったら呼び出される.
    func onOrientationChange(notification: NSNotification){
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView.removeFromSuperview()
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-50, width: 320, height: 50))
        nadView.setNendID("6011089db7691d02c3c9c4e6092c7be61b8714a2",
            spotID: "475395")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}